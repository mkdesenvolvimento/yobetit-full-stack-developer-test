const webpack = require('webpack');

const less = {
  test: /\.(less|css)$/,
  use: [{
    loader: 'style-loader',
  }, {
    loader: 'css-loader',
  }, {
    loader: 'less-loader',
  }],
};

const config = {
  devServer: {
    historyApiFallback: true,
    stats: 'errors-only',
    inline: true,
    host: 'localhost',
    disableHostCheck: true,
  },
  plugins: [
    new webpack.NamedModulesPlugin(),
  ],
  module: {
    rules: [
      less,
    ],
  },
};

module.exports = config;
