import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import {
  BrowserRouter,
  withRouter,
  Route,
  Switch,
} from 'react-router-dom';
import {
  HomeScreen,
  QuestionOneScreen,
  QuestionTwoScreen,
  QuestionThreeScreen,
  QuestionFourScreen,
} from './screens/';

class Container extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const history = this.props.history.listen(this.props.onRouteChange);
    this.setState({ history });
  }

  componentWillUnmount() {
    this.state.history();
  }

  render() {
    return this.props.children;
  }
}

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <Provider store={this.props.store}>
          <BrowserRouter>          
            <Switch>
              <Route exact path="/" component={HomeScreen} />
              <Route exact path="/question-one" component={QuestionOneScreen} />
              <Route exact path="/question-two" component={QuestionTwoScreen} />
              <Route exact path="/question-three" component={QuestionThreeScreen} />
              <Route exact path="/question-four" component={QuestionFourScreen} />
            </Switch>            
          </BrowserRouter>
        </Provider>
    );
  }
}

App.propTypes = {
  store: PropTypes.object.isRequired,
  element: PropTypes.any.isRequired,
};

export default App;
