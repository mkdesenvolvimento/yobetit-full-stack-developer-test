import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  Grid,
  Row,
  Col,
  Button,
  ButtonToolbar,
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock,
  Alert,
} from 'react-bootstrap';
import Navigation from '../../common/Navigation';
import SpinIcon from '../../common/icons/SpinIcon';
import CountryTable from '../../common/CountryTable';
import { getCountryByName, setCountries } from '../../../actions';

const Container = styled.div`
`;

class QuestionOneScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      sending: false,
      validationState: null,
      errorMessage: '',
      disableButton: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.getCountries = this.getCountries.bind(this);
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }

  async getCountries() {
    if (this.state.value.length > 0) {
      this.props.setCountries([]);
      this.setState({
        validationState: 'warning',
        sending: true,
        errorMessage: '',
        disableButton: true,
      });
      try {
        const res = await this.props.getCountryByName(this.state.value, true);
        this.props.setCountries(res);
        this.setState({
          validationState: 'success',
          sending: false,
          disableButton: false,
        });        
      } catch (e) {
        this.setState({
          validationState: 'error',
          sending: false,
          errorMessage: 'No country found.',
          disableButton: false,
        });
      }
    } else {
      this.setState({
        validationState: 'warning',
        errorMessage: 'You must type something.',
      });
    }
  }

  render() {
    let loader = '';
    let errorAlert = '';
    let result = null;
    if (this.state.sending) {
      loader = <SpinIcon size={24} />;
    }
    if (this.state.errorMessage.length > 0) {
      errorAlert = <Alert bsStyle="danger">{this.state.errorMessage}</Alert>;
    }
    if (this.props.countries.length > 0) {
      result = <CountryTable countries={this.props.countries} />;
    }

    return (
      <Container>
        <Navigation activeHref="/question-one" />
        <Grid>
          <Row className="show-grid">
            <Col lg={12}>
              <h1>Question One</h1>
              <FormGroup
                controlId="formBasicText"
                validationState={this.state.validationState}
              >
                <ControlLabel>Type to search the country</ControlLabel>
                <FormControl
                  type="text"
                  value={this.state.value}
                  placeholder="Enter the name of country"
                  onChange={this.handleChange}
                />
                <FormControl.Feedback />
                <HelpBlock>Validation is based on response of request.</HelpBlock>
              </FormGroup>
              <ButtonToolbar>
                <Button
                  onClick={this.getCountries}
                  bsStyle="primary"
                  disabled={this.state.disableButton}
                >Search</Button>
              </ButtonToolbar>
            </Col>
          </Row>
          <hr/>
          <Row className="show-grid">
            <Col lg={12}>
              {loader}
              {errorAlert}         
              {result}
            </Col>
          </Row>

        </Grid>
      </Container>
    );
  }
};

QuestionOneScreen.propTypes = {
  countries: PropTypes.array,
  getCountryByName: PropTypes.func,
  setCountries: PropTypes.func,
};

const mapStateToProps = (state) => ({
  countries: state.countries.countriesData,
});

const mapDispatchToProps = (dispatch) => ({
  getCountryByName: (name, unique) => dispatch(getCountryByName(name, unique)),
  setCountries: (countries) => dispatch(setCountries(countries)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(QuestionOneScreen));