import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  Grid,
  Row,
  Col,
  Button,
  ButtonToolbar,
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock,
  Alert,
} from 'react-bootstrap';
import Navigation from '../../common/Navigation';
import SpinIcon from '../../common/icons/SpinIcon';
import CountryTable from '../../common/CountryTable';
import { getCountryByName, getAllCountries, setCountries } from '../../../actions';
import filterCountries from '../../../helpers/filterCountries';

const Container = styled.div`
`;

class QuestionOneScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 'united brasil',
      sending: false,
      validationState: null,
      errorMessage: '',
      disableButton: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.getCountries = this.getCountries.bind(this);
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }

  async getCountries() {
    if (this.state.value.length > 0) {
      this.props.setCountries([]);
      this.setState({
        validationState: 'warning',
        sending: true,
        errorMessage: '',
        disableButton: true,
      });

      // Now we create the array of string arguments
      const matches = this.state.value.split(' ');

      try {
        const allCountries = await this.props.getAllCountries();
        const matchNames = filterCountries.filterByNames(allCountries, matches);
        if (matchNames && matchNames.length > 0) {
          this.props.setCountries(matchNames)
          this.setState({
            validationState: 'success',
          });
        } else {
          this.setState({
            validationState: 'error',            
            errorMessage: 'No countries found.',            
          })
        }
      } catch (e) {
        this.setState({
          validationState: 'error',
          errorMessage: 'No countries found.',
        })
      }
      this.setState({
        sending: false,
        disableButton: false,
      });      
    } else {
      this.setState({
        validationState: 'warning',
        errorMessage: 'You must type something.',
      });
    }
  }

  render() {
    let loader = '';
    let errorAlert = '';
    let result = null;
    if (this.state.sending) {
      loader = <SpinIcon size={24} />;
    }
    if (this.state.errorMessage.length > 0) {
      errorAlert = <Alert bsStyle="danger">{this.state.errorMessage}</Alert>;
    }
    if (this.props.countries.length > 0) {
      result = <CountryTable countries={this.props.countries} />;
    }

    return (
      <Container>
        <Navigation activeHref="/question-two" />
        <Grid>
          <Row className="show-grid">
            <Col lg={12}>
              <h1>Question Two</h1>
              <FormGroup
                controlId="formBasicText"
                validationState={this.state.validationState}
              >
                <ControlLabel>Type to search the countries</ControlLabel>
                <FormControl
                  type="text"
                  value={this.state.value}
                  placeholder="Enter the countries separated by spaces"
                  onChange={this.handleChange}
                />
                <FormControl.Feedback />
                <HelpBlock>Validation is based on response of request.</HelpBlock>
              </FormGroup>
              <ButtonToolbar>
                <Button
                  onClick={this.getCountries}
                  bsStyle="primary"
                  disabled={this.state.disableButton}
                >Search</Button>
              </ButtonToolbar>
            </Col>
          </Row>
          <hr />
          <Row className="show-grid">
            <Col lg={12}>
              {loader}
              {errorAlert}
              {result}
            </Col>
          </Row>

        </Grid>
      </Container>
    );
  }
};

QuestionOneScreen.propTypes = {
  countries: PropTypes.array,
  getCountryByName: PropTypes.func,
  getAllCountries: PropTypes.func,
  setCountries: PropTypes.func,
};

const mapStateToProps = (state) => ({
  countries: state.countries.countriesData,
});

const mapDispatchToProps = (dispatch) => ({
  getCountryByName: (name, unique) => dispatch(getCountryByName(name, unique)),
  getAllCountries: () => dispatch(getAllCountries()),
  setCountries: (countries) => dispatch(setCountries(countries)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(QuestionOneScreen));