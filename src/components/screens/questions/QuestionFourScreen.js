import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import {
  Grid,
  Row,
  Col,
  Button,
  ButtonToolbar,
  Label,
  Alert,
} from 'react-bootstrap';
import Navigation from '../../common/Navigation';
import spinReels from '../../../helpers/spinReels';

const Container = styled.div`
`;

const Reel = styled.div`
  font-size: 42px;
  float: left;
  margin: 0 0 30px 0;
`;

class QuestionFourScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      coins: 20,
      result: null,
      earnedCoins: 0,
    };
    this.spinTheReel = this.spinTheReel.bind(this);
  }

  spinTheReel() {
    const currentCoins = this.state.coins;
    const results = spinReels.spin();
    this.setState({
      result: results,
    });
    const earnedCoins = spinReels.getPrize(results);    
    this.setState({
      coins: (currentCoins - 1) + earnedCoins,
    });
    this.setState({
      earnedCoins,
    });
  }

  render() {
    let results = null;
    if (this.state.result) {
      const { result } = this.state;
      results = <div>
        <Reel><Label>{result[0]}</Label></Reel>
        <Reel><Label>{result[1]}</Label></Reel>
        <Reel><Label>{result[2]}</Label></Reel>
      </div>;
    }
    let spinButton = <Alert bsStyle="warning">Sorry, you don't have more coins.</Alert>;
    if (this.state.coins > 0) {
      spinButton = <Button
        onClick={this.spinTheReel}
        bsStyle="primary">Spin</Button>;
    }
    let spinAlert = '';
    if (this.state.earnedCoins > 0) {
      spinAlert = <Alert bsStyle="success">
        Congratulations! You've earned <strong>{this.state.earnedCoins} coins</strong>
      </Alert>;
    }

    return (
      <Container>
        <Navigation activeHref="/question-four" />
        <Grid>
          <Row className="show-grid">
            <Col lg={12}>
              <h1>Question Four</h1>
              <h3>Coins: <strong>{this.state.coins}</strong></h3>
              <ButtonToolbar>
                {spinButton}
              </ButtonToolbar>
            </Col>
          </Row>
          <hr />
          <Row className="show-grid">
            <Col lg={12}>
              {results}
            </Col>
          </Row>
          <Row className="show-grid">
            <Col lg={12}>
              {spinAlert}
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
};

const mapStateToProps = (state) => ({});
const mapDispatchToProps = (dispatch, ownProps) => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(QuestionFourScreen));