import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  Grid,
  Row,
  Col,
  Button,
  ButtonToolbar,
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock,
  Alert,
} from 'react-bootstrap';
import Navigation from '../../common/Navigation';
import SpinIcon from '../../common/icons/SpinIcon';
import CountryTable from '../../common/CountryTable';
import { getCountryByName, getAllCountries, setCountries } from '../../../actions';
import filterCountries from '../../../helpers/filterCountries';

const Container = styled.div`
`;

class QuestionThreeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      sending: true,
      validationState: null,
      errorMessage: '',
      disableButton: true,
      allCountries: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.getCountries = this.getCountries.bind(this);
    this.resetSearch = this.resetSearch.bind(this);
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }

  getCountries() {
    if (this.state.value.length > 0) {
      this.setState({
        validationState: 'warning',
        sending: true,
        errorMessage: '',
        disableButton: true,
      });

      const { countries } = this.props;

      try {
        const matchNames = filterCountries.filterByNames(countries, this.state.value);
        if (matchNames && matchNames.length > 0) {
          this.setState({
            validationState: 'success',
            allCountries: matchNames,
          });
        } else {
          this.setState({
            validationState: 'error',
            errorMessage: 'No countries found.',
          })
        }
      } catch (e) {
        this.setState({
          validationState: 'error',
          errorMessage: 'No countries found.',
        })
      }
      this.setState({
        sending: false,
        disableButton: false,
      });
    } else {
      this.setState({
        validationState: 'warning',
        errorMessage: 'You must type something.',
      });
    }
  }

  resetSearch() {
    this.setState({
      allCountries: this.props.countries,
      value: '',
      validationState: null,
      errorMessage: '',
    });
  }

  async componentDidMount() {
    try {        
      const allCountries = await this.props.getAllCountries();
      this.setState({
        sending: false,
        disableButton: false,
      })
      this.props.setCountries(allCountries);
    } catch (e) {
      this.setState({
        sending: false,
        disableButton: false,
        validationState: 'error',
        errorMessage: 'No countries found.',
      })
    }
  }

  render() {
    let loader = '';
    let errorAlert = '';
    let result = null;
    if (this.state.sending) {
      loader = <SpinIcon size={24} />;
    }
    if (this.state.errorMessage.length > 0) {
      errorAlert = <Alert bsStyle="danger">{this.state.errorMessage}</Alert>;
    }
    if (this.props.countries.length > 0) {
      result = <CountryTable countries={
        this.state.allCountries.length === 0 ? this.props.countries : this.state.allCountries } />;
    }

    return (
      <Container>
        <Navigation activeHref="/question-three" />
        <Grid>
          <Row className="show-grid">
            <Col lg={12}>
              <h1>Question Three</h1>
              <FormGroup
                controlId="formBasicText"
                validationState={this.state.validationState}
              >
                <ControlLabel>Type to filter the countries</ControlLabel>
                <FormControl
                  type="text"
                  value={this.state.value}
                  placeholder="Enter the country name"
                  onChange={this.handleChange}
                />
                <FormControl.Feedback />
                <HelpBlock>Validation is based on response of request.</HelpBlock>
              </FormGroup>
              <ButtonToolbar>
                <Button
                  onClick={this.getCountries}
                  bsStyle="primary"
                  disabled={this.state.disableButton}
                >Filter</Button>
                <Button
                  onClick={this.resetSearch}
                  bsStyle="default"
                >Reset</Button>
              </ButtonToolbar>
            </Col>
          </Row>
          <hr />
          <Row className="show-grid">
            <Col lg={12}>
              {loader}
              {errorAlert}
              {result}
            </Col>
          </Row>

        </Grid>
      </Container>
    );
  }
};

QuestionThreeScreen.propTypes = {
  countries: PropTypes.array,
  getCountryByName: PropTypes.func,
  getAllCountries: PropTypes.func,
  setCountries: PropTypes.func,
};

const mapStateToProps = (state) => ({
  countries: state.countries.countriesData,
});

const mapDispatchToProps = (dispatch) => ({
  getCountryByName: (name, unique) => dispatch(getCountryByName(name, unique)),
  getAllCountries: () => dispatch(getAllCountries()),
  setCountries: (countries) => dispatch(setCountries(countries)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(QuestionThreeScreen));