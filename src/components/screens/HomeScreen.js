import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import {
  Grid,
  Row,
  Col,
} from 'react-bootstrap';
import Navigation from '../common/Navigation';

const Container = styled.div`
`;

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillUnmount() { }
  componentDidMount() { }

  render() {
    return (
      <Container>
        <Navigation activeHref='/' />
        <Grid>
          <Row className="show-grid">
            <Col lg={12}>
              <h1>Home Screen</h1>
              <p>This is just a simple home screen and navigation between the questions.</p>
              <p>I hope you like my test. ;)</p>
              <p><strong>Markus Ethur</strong></p>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
};

const mapStateToProps = (state) => ({});
const mapDispatchToProps = (dispatch, ownProps) => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HomeScreen));