import HomeScreen from './HomeScreen';
import QuestionOneScreen from './questions/QuestionOneScreen';
import QuestionTwoScreen from './questions/QuestionTwoScreen';
import QuestionThreeScreen from './questions/QuestionThreeScreen';
import QuestionFourScreen from './questions/QuestionFourScreen';

export {
  HomeScreen,
  QuestionOneScreen,
  QuestionTwoScreen,
  QuestionThreeScreen,
  QuestionFourScreen,
};