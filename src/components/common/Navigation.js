import React from 'react';
import {
  Navbar,
  Nav,
  NavItem,
} from 'react-bootstrap';

const Navigation = (props) => {
  return <Navbar 
          inverse
          collapseOnSelect
          fluid
          staticTop
         >
    <Navbar.Header>
      <Navbar.Brand>
        <a href="/">Home</a>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav activeHref={props.activeHref || null}>
        <NavItem eventKey={1} href="/question-one">
          Question One
        </NavItem>
        <NavItem eventKey={2} href="/question-two">
          Question Two
        </NavItem>
        <NavItem eventKey={3} href="/question-three">
          Question Three
        </NavItem>
        <NavItem eventKey={4} href="/question-four">
          Question Four
        </NavItem>
      </Nav>
    </Navbar.Collapse>
  </Navbar>;
};

export default Navigation;
