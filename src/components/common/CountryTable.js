import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
} from 'react-bootstrap';

const CountryTable = (props) => {
  let rows = '';
  if (props.countries && props.countries.length > 0) {
    const { countries } = props;
    rows = countries.map((value, index) => {
      return <tr key={index}>
        <td>{value.cioc}</td>
        <td>{value.name}</td>
        <td>{value.nativeName}</td>
        <td>{value.region}</td>
      </tr>;
    });
  }
  const tableResults = <Table striped bordered condensed hover>
    <thead>
      <tr>
        <th>CIOC</th>
        <th>Name</th>
        <th>Native Name</th>
        <th>Region</th>
      </tr>
    </thead>
    <tbody>
      {rows}
    </tbody>
  </Table>;

  return tableResults;
};

CountryTable.propTypes = {
  countries: PropTypes.array,
};

export default CountryTable;
