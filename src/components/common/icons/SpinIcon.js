import React from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes, css } from 'styled-components';

const rotate = keyframes`
  0% { transform: rotate(0deg); }
  12.5% { transform: rotate(45deg); }
  25% { transform: rotate(90deg); }
  37.5% { transform: rotate(135deg); }
  50% { transform: rotate(180deg); }
  62.5% { transform: rotate(225deg); }
  75% { transform: rotate(270deg); }
  87.5% { transform: rotate(315deg); }
  100% { transform: rotate(360deg); }
`;

const Rotator = styled.span`
  display: inline-block;
  animation: ${rotate} 1s step-start infinite;
`;

export default function SpinIcon(props) {
  const color = props.color || '#A8A8A8';
  const path = [
    <circle key="1" fill={color} cx="42" cy="9" r="9" opacity="0.3"></circle>,
    <circle key="2" fill={color} cx="65" cy="19" r="9" opacity="0.4"></circle>,
    <circle key="3" fill={color} cx="74" cy="42" r="9" opacity="0.5"></circle>,
    <circle key="4" fill={color} cx="65" cy="65" r="9" opacity="0.6"></circle>,
    <circle key="5" fill={color} cx="41" cy="74" r="9" opacity="0.7"></circle>,
    <circle key="6" fill={color} cx="18" cy="64" r="9" opacity="0.8"></circle>,
    <circle key="7" fill={color} cx="9" cy="41" r="9" opacity="0.9"></circle>,
    <circle key="8" fill={color} cx="19" cy="18" r="9" opacity="1"></circle>,
  ]; // eslint-disable-line

  return <Rotator className='spin-icon'>
    <svg version="1.1"
      width={props.size || '83'}
      height={props.size || '83'}
      fill="none"
      viewBox="0 0 83 83">
      {path}
    </svg>
  </Rotator>;
}

SpinIcon.propTypes = {
  size: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  color: PropTypes.string,
};
