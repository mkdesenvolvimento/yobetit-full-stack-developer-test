import apiService from '../helpers/apiService';

export const setCountries = (countries) => {
  return {
    type: 'SET_COUNTRIES',
    countries,
  };
};

export const getCountryByName = (name, unique = false) => {
  return async () => {
    const res = await apiService.getCountryByName(name, unique).catch((err) => {
      throw err;
    });
    return res;
  };
};

export const getAllCountries = () => {
  return async () => {
    const res = await apiService.getAllCountries().catch((err) => {
      throw err;
    });
    return res;
  };  
}

