export default (state = {
  countriesData: [],
}, action) => {
  switch (action.type) {
    case 'SET_COUNTRIES':
      return { ...state, countriesData: action.countries };
    default:
      return state;
  }
};
