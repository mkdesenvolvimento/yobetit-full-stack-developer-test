import { combineReducers } from 'redux';
import countries from './countries';

const appReducer = combineReducers({
  countries,
});

export default appReducer;

