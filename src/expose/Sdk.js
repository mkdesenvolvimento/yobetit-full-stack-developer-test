import React from 'react';
import thunkMiddleware from 'redux-thunk';
import { render, unmountComponentAtNode } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import appReducer from '../reducers';
import App from '../components/App';

const store = createStore(appReducer, applyMiddleware(thunkMiddleware));

export default class Sdk {
  static start(element) {
    if (!element) {
      throw new Error('element is required');
    }

    render(<App store={store} element={element} />, element);

    return {
      destroy: () => {
        element.removeChild(element.childNodes[0]);
        unmountComponentAtNode(element);
      },
    }
  }
}

