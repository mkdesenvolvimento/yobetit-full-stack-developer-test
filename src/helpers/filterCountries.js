export default {
  filterByNames(countries, matches) {
    const results = [];
    if (countries.length > 0) {
      for (let x = 0; x < countries.length; x += 1) {
        const { name, nativeName } = countries[x];
        if (typeof matches === 'object') {
          for (let y = 0; y < matches.length; y += 1) {
            const match = matches[y];
            if (name.toLowerCase().includes(match) || nativeName.toLowerCase().includes(match)) {
              results.push(countries[x]);
            }
          }
        } else if (typeof matches === 'string') {
          if (name.toLowerCase().includes(matches) || nativeName.toLowerCase().includes(matches)) {
            results.push(countries[x]);
          }
        }
      }
    }
    return results;
  },
};
