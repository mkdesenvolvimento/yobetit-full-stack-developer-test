import axios from 'axios';
import http from './http';

export default {
  http,
  getCountryByName: async (name, unique) => {
    let params = '';
    if (unique) {
      params = '?fullText=true';
    }
    const res = await axios.get(`https://restcountries.eu/rest/v2/name/${name}${params}`).catch((err) => {
      throw err;
    });
    return res.data;
  },
  getAllCountries: async () => {
    const res = await axios.get(`https://restcountries.eu/rest/v2/all`).catch((err) => {
      throw err;
    });
    return res.data;
  },
};


