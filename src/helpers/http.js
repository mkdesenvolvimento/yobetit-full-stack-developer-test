import axios from 'axios';

const http = axios.create({
  timeout: 30000,
  headers: {},
});

http.interceptors.response.use((response) => {
  return response;
}, (err) => {
  if (err.response) {
    const { error: responseError } = err.response.data;
    if (responseError) {
      err.message = responseError.message;
    }
  }
  throw err;
});

export default http;
