export default {
  spin() {
    const reels = [
      ['cherry', 'lemon', 'apple', 'lemon', 'banana', 'banana', 'lemon', 'lemon'],
      ['lemon', 'apple', 'lemon', 'lemon', 'cherry', 'apple', 'banana', 'lemon'],
      ['lemon', 'apple', 'lemon', 'apple', 'cherry', 'lemon', 'banana', 'lemon'],
    ];
    const result = [];
    for (let x = 0; x < reels.length; x += 1) {
      const reel = reels[x];
      const luckyReel = Math.floor(Math.random() * 8);
      result.push(reel[luckyReel]);
    }
    return result;
  },
  getPrize(results) {
    const options = {
      cherries: 0,
      apples: 0,
      bananas: 0,
      lemons: 0,
    };
    let coins = 0;

    for (let x = 0; x < results.length; x += 1) {
      const result = results[x];
      if (result === 'cherry') {
        options.cherries += 1;
      }
      if (result === 'banana') {
        options.bananas += 1;
      }
      if (result === 'apple') {
        options.apples += 1;
      }
      if (result === 'lemon') {
        options.lemons += 1;
      }
    }

    if (options.cherries === 3) {
      coins = 50;
    } else if (options.cherries === 2) {
      coins = 40;
    } else if (options.apples === 3) {
      coins = 20;
    } else if (options.apples === 2) {
      coins = 10;
    } else if (options.bananas === 3) {
      coins = 15;
    } else if (options.bananas === 2) {
      coins = 5;
    } else if (options.lemons === 3) {
      coins = 3;
    }

    return coins;
  },
};
