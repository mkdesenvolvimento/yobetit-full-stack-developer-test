CREATE DATABASE `casinos_db`;

USE `casinos_db`;


CREATE TABLE `casino` (

`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` varchar(60) NULL,

PRIMARY KEY (`id`) 

);



CREATE TABLE `games` (

`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`casino_id` int(11) UNSIGNED NOT NULL,

`gametype_id` int(11) UNSIGNED NOT NULL,

`name` varchar(60) NOT NULL,

PRIMARY KEY (`id`) 

);



CREATE TABLE `gameTypes` (

`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` varchar(60) NOT NULL,

PRIMARY KEY (`id`) 

);



CREATE TABLE `gameAllowedCountry` (

`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`game_id` int(11) UNSIGNED NOT NULL,

`country_id` int(11) UNSIGNED NOT NULL,

PRIMARY KEY (`id`) 

);



CREATE TABLE `countries` (

`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` varchar(60) NOT NULL,

PRIMARY KEY (`id`) 

);



CREATE TABLE `players` (

`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` varchar(100) NOT NULL,

`favorite_gametype_id` int(11) UNSIGNED NULL,

PRIMARY KEY (`id`) 

);





ALTER TABLE `games` ADD CONSTRAINT `fk_games_casino_1` FOREIGN KEY (`casino_id`) REFERENCES `casino` (`id`);

ALTER TABLE `games` ADD CONSTRAINT `fk_games_gameTypes_1` FOREIGN KEY (`gametype_id`) REFERENCES `gameTypes` (`id`);

ALTER TABLE `gameAllowedCountry` ADD CONSTRAINT `fk_gameAllowedCountry_countries_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`);

ALTER TABLE `gameAllowedCountry` ADD CONSTRAINT `fk_gameAllowedCountry_games_1` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`);

ALTER TABLE `players` ADD CONSTRAINT `fk_players_gameTypes_1` FOREIGN KEY (`favorite_gametype_id`) REFERENCES `gameTypes` (`id`);



